package Demo;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import com.jaunt.Element;
import com.jaunt.Elements;
import com.jaunt.UserAgent;

public class TestJaunt {
	// log4
	// proxy javass

	public static void main(String arg[]) throws Exception {
		
		String strLinkFileProxy="D:\\thuctap_datasection\\proxy.txt";
		String strLinkFileKey="D:\\thuctap_datasection\\KeySearch.txt";
		String strLinkFileWrite="D:\\thuctap_datasection\\Jaunt.txt";
		
		searchKey(strLinkFileProxy,strLinkFileKey,strLinkFileWrite);
	}
	
	public static void searchKey(String strLinkFileProxy, String strLinkFileKey,String strLinkFileWrite) throws Exception {
		File fileA = new File(strLinkFileProxy);
		File fileB = new File(strLinkFileKey);

		List<String> StringKey = FileUtils.readLines(fileB, "UTF-8");
		List<String> hostports = FileUtils.readLines(fileA, "UTF-8"); // doc tung dong du lieu tu file
		for (String hostport : hostports) {
			String host = hostport.split(":")[0];
			int port = Integer.parseInt(hostport.split(":")[1]);
			// ghi log
			for (String key : StringKey) {
				System.out.println(key);				
				getLink(key, host, port,strLinkFileWrite);
				writeFile(key + "\t nextKey",strLinkFileWrite);
			}
		}
		System.out.println("ok");
	}
	
	public static void getLink(String strKey, String strHost, int iPort,String strLinkFileWrite) throws Exception {
		UserAgent uA = new UserAgent();
		uA.setProxyHost(strHost);
		uA.setProxyPort(iPort);
		try {
			uA.visit("https://www.google.com.vn");
			uA.doc.apply(strKey);
			uA.doc.submit();
			for (int i = 0; i < 50; i++) {
				writeFile("Next page:\n",strLinkFileWrite);
				UserAgent userAgent = null;
				try {
					userAgent = new UserAgent();
					userAgent.visit("https://www.google.com.vn/search?q=" + strKey + "&start=" + i * 10);
					Elements elements = null;
					if (userAgent.doc.findEvery("<h3>").findEvery("<a href") != null) {
						elements = userAgent.doc.findEvery("<h3>").findEvery("<a href");
						for (Element element : elements) {
							writeFile(element.getAtString("href"),strLinkFileWrite);
						}
					} else {
						break;
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					// ghi log
				}
				writeFile("\n",strLinkFile);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// ghi ra file
	public static void writeFile(String string,String strLinkFile) throws Exception {
		FileOutputStream fos = new FileOutputStream(strLinkFile, true);
		PrintWriter pw = new PrintWriter(fos);
		pw.println(string);
		pw.close();
		fos.flush();
		fos.close();
	}

}

// tìm hiểu về proxy, logg, cài ubuntu
